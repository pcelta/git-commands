
Para adicionar arquivos que serão ignorados de forma global em todos os projetos.

Primeiro é necessário criar um arquivo que conterá as regras de ignore.

No meu caso: ~/.config/gitignore_global

com o seguinte conteúdo;

# eclipse configs
.externalToolBuilders/*


Isso fará com que os arquivos de configuração que o Eclipse IDE cria, não será versionado pelo Git

Depois de criado o arquivo é só executar o seguinte comando:

git config --global core.excludesfile ~/.config/gitignore_global
